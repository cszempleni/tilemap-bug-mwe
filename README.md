Only occlusion layer 0 seems to be deleted at TileMap cleanup

When using multiple occlusion layers in TleiMap, it sems that upon destruction only the occlusion layer 0 is deleted, the other aren't.

Reason for using multiple occlusion layers: https://ask.godotengine.org/50118/how-do-you-use-multiple-polygons-for-occlusion-with-autotile
I needed multiple occlusion polygons, but because only one is supported per tile per layer, I decided to create more layers, to have the same effect like multiple polygons.

Godot version: 4.1.3
System information: Linux, Android

Reproduction steps:
1. Create a TileMap with a TileSet with more than one occlusion layers
2. Instantiate a scene with the TileMap, with some terrains with the multiple occlusion layers
   This works fine
3. Free the previously generated TileMap
4. Create the scene again - with a different terrain

Effect: the previously generated map's occlusion layer 1, 2, 3, 4... is still visible - only layer 0 was deleted

For easier reproduction see this project link:
