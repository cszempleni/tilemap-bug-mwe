extends Node2D

signal finished()

func start(map_number : int):
	$Level.start(map_number)
	$Level.place_player($Player)

func _input(event):
	if event.is_pressed():
		finished.emit()
