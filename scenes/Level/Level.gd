extends Node2D

@export var number_of_rows = 7
@export var number_of_columns = 7

var start_coordinate = Vector2i(number_of_columns / 2, number_of_rows / 2)

func start(map_number : int):
	create_map(map_number)

func place_player(player : Node2D):
	var local_coordinates = $TileMap.map_to_local(start_coordinate)
	player.position = local_coordinates

func create_map(map_number : int):
	$TileMap.clear()
	create_floors()
	draw_map(map_number)
	
func create_floors():
	var the_path = []
	
	for column in number_of_columns:
		for row in number_of_rows:
			the_path.append(Vector2i(column, row))
	
	$TileMap.set_cells_terrain_connect(0, the_path, 0, 1, false)

func draw_map(map_number : int):
	var the_path : Array[Vector2i] = []

	if map_number == 1:
		the_path = generate_first_map()
	elif map_number == 2:
		the_path = generate_second_map()

	$TileMap.set_cells_terrain_connect(0, the_path, 0, 0, false)

func generate_first_map():
	var the_path : Array[Vector2i] = []
	for column in range(start_coordinate.x - 1, start_coordinate.x + 1 + 1):
		the_path.append(Vector2i(column, start_coordinate.y - 1))
	for column in range(start_coordinate.x - 1, start_coordinate.x + 1 + 1):
		the_path.append(Vector2i(column, start_coordinate.y + 1))
	return the_path

func generate_second_map():
	var the_path : Array[Vector2i] = []
	for row in range(start_coordinate.y - 1, start_coordinate.y + 1 + 1):
		the_path.append(Vector2i(start_coordinate.x - 1, row))
	for row in range(start_coordinate.y - 1, start_coordinate.y + 1 + 1):
		the_path.append(Vector2i(start_coordinate.x + 1, row))
	return the_path
