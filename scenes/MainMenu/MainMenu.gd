extends Control

var world_scene = preload("res://scenes/World/World.tscn")

var the_world

func create_map_button_pressed(map_number : int):
	$MainMenu.hide()
	the_world = world_scene.instantiate()
	the_world.finished.connect(world_finished)
	the_world.start(map_number)
	the_world.show()
	add_child.call_deferred(the_world)

func world_finished():
	the_world.queue_free()
	$MainMenu.show()
